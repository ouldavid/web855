<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

// define global variable
view()->share('version', time());
view()->share('host_name', '855 Lottery');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/result', 'ResultController@index')->name('result');
Route::get('/agent', 'AgentController@index')->name('agent');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/csr', 'CsrController@index')->name('csr');
Route::get('/about-us', 'AboutController@index')->name('about-us');

// user register
/*Route::get('/register', 'RegisterController---custom@index')->name('register');
Route::get('/register', 'RegisterController---custom@register');*/


// user login
Route::get('/user', 'UserController@index')->name('user');
Route::get('/user/ajax-aba', 'UserController@ajaxAba')->name('ajax-aba');
Route::get('/user/ajax-aceleda', 'UserController@ajaxAceleda')->name('ajax-aceleda');

// Betting Page
Route::get('/betting', 'BettingController@index')->name('betting');
Route::get('/ajax-bet','BettingController@ajaxBet')->name('ajax-bet');
Route::post('/ajax-bet','BettingController@store')->name('store-bet');
Route::get('/ajax-onChange','BettingController@ajaxOnChange')->name('ajax-onChange');
Route::get('/ajax-ticket','BettingController@ajaxTicket')->name('ajax-ticket');

// Result Page
Route::get('/result', 'ResultController@index')->name('result');
Route::get('/result/show','ResultController@show')->name('result.show');
