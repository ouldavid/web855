@extends('layouts.master')

@section('title', ucfirst($view_name))

@section('css')
    {{--<!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($view_name).'.css?v='.$version) }}">--}}
@endsection

@section('content')

    <h1 class="text-center" style="height: 300px">COMING SOON!</h1>

@endsection

@section('script')
    {{--<!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>--}}
@endsection
