@extends('layouts.master')

@section('title', ucfirst($view_name))

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css?v='.$version) }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($view_name).'.css?v='.$version) }}">
@endsection

<!-- include banner -->
@section('banner')
    <div id="banner-slide">
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-01.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-01-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
            <button type="button" class="btn register">Play Now</button>
        </div>
        <div class="slide">
            <div class="bg-slide" style="background-image: url({{ asset('img/banner/slide-01.jpg?v='.$version) }})"></div>
            <img class="img-fluid img-slide" src="{{ asset('img/banner/slide-01-mobile.jpg?v='.$version) }}" alt="{{ $host_name }}" />
            <button type="button" class="btn register">Play Now</button>
        </div>
    </div>
    <hr>
@endsection

@section('content')
    <div class="container m-y-30">
        <div id="lotteryType" class="row">
            <div class="col-md-4 col-sm-12 col-12 mb-3">
                <img class="img-fluid" src="{{ asset('img/lottery-vn.jpg?v='.$version) }}" alt="{{ $host_name }}" />
                <div class="d-flex justify-content-center">
                    <button type="button" class="btn register">Play Now</button>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12 mb-3">
                <img class="img-fluid" src="{{ asset('img/lottery-kh.jpg?v='.$version) }}" alt="{{ $host_name }}" />
                <div class="d-flex justify-content-center">
                    <button type="button" class="btn register">Play Now</button>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12 mb-3">
                <img class="img-fluid" src="{{ asset('img/lottery-th.jpg?v='.$version) }}" alt="{{ $host_name }}" />
                <div class="d-flex justify-content-center">
                    <button type="button" class="btn register">Play Now</button>
                </div>
            </div>
        </div>

        <div id="lotteryLatest" class="mb-5">
            <h1>Latest Lottery Results</h1>
            <small>Check your result lotto online, find all the lotto winning numbers and see if you won the latest lotto jackpots!</small>

            <ul id="tabs" class="nav nav-tabs">
                <li class="nav-item"><a href="" data-target="#vietnam" data-toggle="tab" class="nav-link text-uppercase active">vietnam</a></li>
                <li class="nav-item"><a href="" data-target="#khmer" data-toggle="tab" class="nav-link text-uppercase">khmer</a></li>
                <li class="nav-item"><a href="" data-target="#thai" data-toggle="tab" class="nav-link text-uppercase">thai</a></li>
            </ul>
            <div id="tabsContent" class="tab-content">
                <div id="vietnam" class="tab-pane fade active show">
                    <table class="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                            <th scope="col">ម៉ោង</th>
                            <th scope="col">ប្រភេទ</th>
                            <th scope="col">A</th>
                            <th scope="col">B</th>
                            <th scope="col">C</th>
                            <th scope="col">D</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <!-- noon -->
                            <td rowspan="0" class="text-center">07.01.2020</td>
                            <td rowspan="4" class="text-center">4:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/monkey.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/horse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <!-- evening -->
                            <td rowspan="4" class="align-middle text-center">7:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/snake.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dog.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dragon.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/mouse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="khmer" class="tab-pane fade">
                    <table class="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                            <th scope="col">ម៉ោង</th>
                            <th scope="col">ប្រភេទ</th>
                            <th scope="col">A</th>
                            <th scope="col">B</th>
                            <th scope="col">C</th>
                            <th scope="col">D</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <!-- noon -->
                            <td rowspan="0" class="text-center">07.01.2020</td>
                            <td rowspan="4" class="text-center">4:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/monkey.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/horse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <!-- evening -->
                            <td rowspan="4" class="align-middle text-center">7:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/snake.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dog.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dragon.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/mouse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="thai" class="tab-pane fade">
                    <table class="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                            <th scope="col">ម៉ោង</th>
                            <th scope="col">ប្រភេទ</th>
                            <th scope="col">A</th>
                            <th scope="col">B</th>
                            <th scope="col">C</th>
                            <th scope="col">D</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <!-- noon -->
                            <td rowspan="0" class="text-center">07.01.2020</td>
                            <td rowspan="4" class="text-center">4:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/monkey.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/horse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <!-- evening -->
                            <td rowspan="4" class="align-middle text-center">7:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/snake.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dog.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dragon.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/mouse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="#" class="moreResult">See all result</a>
        </div>

        <div id="winStep" class="row">
            <div class="col-12">
                <h1 class="text-center">Easy 3 Steps To Win</h1>
                <small class="d-block text-center">Check your result lotto online, find all the lotto winning numbers and see if you won the latest lotto jackpots!</small>
                <img class="img-fluid" src="{{ asset('img/win_step.jpg?v='.$version) }}" alt="Easy step to win" />
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js?v='.$version) }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>
@endsection
