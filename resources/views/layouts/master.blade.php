<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
        <meta name="format-detection" content="telephone=no" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') | 855 Lottery</title>

        <meta name="description" content="" />
        <meta name="description" itemprop="description" content="" />
        <meta name="keywords" content="" />
        <meta name="keywords" itemprop="keywords" content="" />
        <meta name="author" content="" />

        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="" />
        <meta itemprop="description" content="" />
        <meta itemprop="image" content="" />

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="" />
        <meta name="twitter:creator" content="" />
        <meta name="twitter:domain" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:title" content="" />
        <meta name="twitter:description" content="" />
        <meta name="twitter:image" content="" />

        <!-- FB meta -->
        <meta property="fb:pages" content="" />
        <meta property="fb:app_id" content="" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="" />
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:image" content="" />
        <meta property="og:image:url" content="" />

        <!-- Canonical -->
        <link rel="canonical" href="" />

        <!-- Favicon -->
        <link rel="shortcut icon" href="/favicon.ico?v={{ $version }}" type="image/x-icon" />

        <!-- Import CSS -->
        <link href="{{ asset('css/app.css?v='.$version) }}" rel="stylesheet" type="text/css" />
        <!-- Flag icon -->
        <link href="{{ asset('components/flag-icon-css/css/flag-icon.min.css?v='.$version) }}" rel="stylesheet" type="text/css" />
        <!-- bootstrap4 glyphicons -->
        <link href="{{ asset('components/bootstrap4-glyphicons/css/bootstrap-glyphicons.min.css?v='.$version) }}" rel="stylesheet" type="text/css" />
        <!-- Clear CSS -->
        <link href="{{ asset('css/clear.css?v='.$version) }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/common.css?v='.$version) }}" rel="stylesheet" type="text/css" />
        @yield('css')

        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115551422-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-115551422-1');
        </script>-->
        <!-- End Google Analytics -->

        <!-- share social button -->
        <!--<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a90e413992ac400137609bf&product=inline-share-buttons' async='async'></script>-->

    </head>
    <body>
        <header>
            @include('templates.header')
        </header>

        <!-- banner slide -->
        @yield('banner')

        <!-- content -->
        @yield('content')

        <footer>
            @include('templates.footer')
        </footer>

        <!-- Import JS -->
        <script src="{{ asset('js/app.js?v='.$version) }}" ></script>
        <script type="text/javascript" src="{{ asset('js/common.js?v='.$version) }}"></script>

        @yield('script')
    </body>
</html>
