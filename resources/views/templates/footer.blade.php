<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="paymentMethod">
                <h1>Payment Method</h1>
                <p>Buy international tickets online using any of the payment methods</p>
                <div class="icon-container text-center">
                    <i class="icon-img"><img src="{{ asset('img/card/small-visa.png') }}" alt="Visa Card" /></i>
                    <i class="icon-img"><img src="{{ asset('img/card/small-master.png') }}" alt="Master Card" /></i>
                    <i class="icon-img"><img src="{{ asset('img/card/small-pipay.png') }}" alt="Pi Pay" /></i>
                    <i class="icon-img"><img src="{{ asset('img/card/small-true.png') }}" alt="True Money" /></i>
                    <i class="icon-img"><img src="{{ asset('img/card/small-wing.png') }}" alt="Wing" /></i>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="container" id="footerBottom">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-12 mb-3 d-flex justify-content-center text-xs-center">
            <ul>
                <h2>{{ $host_name }}</h2>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">About us</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">How it works</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Our services</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Blog</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Contact us</a></li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-6 col-12 mb-3 d-flex justify-content-center text-xs-center">
            <ul>
                <h2>Quick Links</h2>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">My account</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Affiliate program</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Terms & Conditions</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">Privacy</a></li>
                <li><i class="glyphicon glyphicon-triangle-right"></i> <a href="#">{{ $host_name }} Licenses</a></li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-12 col-12 mb-3 d-flex justify-content-center text-xs-center">
            <ul>
                <h2>Email Newsletters</h2>
                <li><p>Subscribe new and receive weekly newsletter for latest draw and offer news and much more!</p></li>
                <li class="mt-3">
                    <ul class="social-network social-circle">
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
