<nav class="navbar navbar-expand-lg navbar-dark bg-dark sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/"><img class="img-fluid" src="{{ asset('img/logo/logo.png') }}" alt="{{ $host_name }}"></a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="nav navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link" href="/"><i class="fas fa-home"></i> Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('betting') }}">BETTING</a>
                </li>
                {{--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">RESULT</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">VIETNAM</a>
                        <a class="dropdown-item" href="#">KHMER</a>
                        <a class="dropdown-item" href="#">THAI</a>
                    </div>
                </li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('result') }}">RESULT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('agent') }}">AGENT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('blog') }}">BLOG</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('csr') }}">CSR</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about-us') }}">ABOUT US</a>
                </li>
            </ul>
            {{--<form class="form-inline my-2 my-lg-0">
                 <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                 <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
             </form> --}}
        </div>
        <div id="nav-right">
        @if(Auth::check())
            <!-- login -->
            <ul class="login">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-user rounded-circle"></span></a>
                    <div class="dropdown-menu" aria-labelledby="dropdown07">
                        <a class="dropdown-item" href="/user#profile">My Account</a>
                        <a class="dropdown-item" href="/user#deposit">Deposit</a>
                        <a class="dropdown-item" href="/user#withdraw">Withdraw</a>
                        {{--<a class="dropdown-item" href="#">Transfer</a>
                        <a class="dropdown-item" href="#">Bet Records</a>--}}
                        <a class="dropdown-item" href="/user#statement">Statements</a>
                        <a class="dropdown-item" href="/user#bonus">Bonus</a>
                        {{--<a class="dropdown-item logout" href="#">Log Out</a>--}}
                        <a class="dropdown-item logout" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="nav-link">
                        <h6>{{ ucwords(auth()->user()->name) }}</h6>
                        <small>(50.00 USD)</small>
                    </div>
                </li>
            </ul>
        @else
            <!-- no login -->
            <ul>
                <li><a href="{{ route('login') }}" class="btn sign-in"><i class="fas fa-user"></i> Sign in</a></li>
                <li><a href="{{ route('register') }}" class="btn register">Join us</a></li>
                {{--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flag-icon flag-icon-kh"> </span> KH</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                        <a class="dropdown-item" href="#vn"><span class="flag-icon flag-icon-vn"> </span>  VN</a>
                    </div>
                </li>--}}
            </ul>
        @endif
        </div>
    </div>
</nav>
