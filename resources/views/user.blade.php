@extends('layouts.master')

@section('title', ucfirst($view_name))

@section('css')
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('components/bootstrap-daterangepicker/daterangepicker.css?v='.$version) }}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('components/bootstrap-datepicker/bootstrap-datepicker.min.css?v='.$version) }}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ asset('components/bootstrap-timepicker/bootstrap-timepicker.min.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($view_name).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container m-y-30">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h1>Welcome to Latest Lottery</h1>
                <small>Check your result lotto online, find all the lotto winning numbers and see if you won the latest lotto jackpots!</small>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="bgbox-shadow shadow clearfix">
                    <!-- tabs -->
                    <ul class="nav nav-tabs responsive-tabs" id="myTab">
                        <li class="nav-item"><a href="#profile" class="nav-link text-capitalize active" data-toggle="tab">Basic Information</a></li>
                        <li class="nav-item"><a href="#password" class="nav-link text-capitalize" data-toggle="tab">Change Password</a></li>
                        <li class="nav-item"><a href="#deposit" class="nav-link text-capitalize" data-toggle="tab">Deposit</a></li>
                        <li class="nav-item"><a href="#withdraw" class="nav-link text-capitalize" data-toggle="tab">Withdraw</a></li>
                        <li class="nav-item"><a href="#statement" class="nav-link text-capitalize" data-toggle="tab">Statements</a></li>
                        <li class="nav-item"><a href="#bonus" class="nav-link text-capitalize" data-toggle="tab">Bonus</a></li>
                    </ul>

                    <div class="tab-content">
                        <!-- profile -->
                        <div class="tab-pane active show" id="profile">
                            <table class="table table-borderless w-auto">
                                <tbody>
                                <tr>
                                    <td>Account</td>
                                    <td><span class="pr-3">:</span> Yako ZinII</td>
                                </tr>
                                <tr>
                                    <td>Ref Link</td>
                                    <td><span class="pr-3">:</span> https://example.com</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td><span class="pr-3">:</span> 010817163</td>
                                </tr>
                                <tr>
                                    <td>Withdraw Bank Account Name</td>
                                    <td><span class="pr-3">:</span> *** ZinII</td>
                                </tr>
                                <tr>
                                    <td>Withdraw Balance No.</td>
                                    <td><span class="pr-3">:</span> ******163</td>
                                </tr>
                                <tr>
                                    <td>Withdraw Bank</td>
                                    <td><span class="pr-3">:</span> ABA</td>
                                </tr>
                                <tr>
                                    <td>w88 APP ID</td>
                                    <td><span class="pr-3">:</span> 12345</td>
                                </tr>
                                <tr>
                                    <td>Total Bonus</td>
                                    <td><span class="pr-3">:</span> 0.00</td>
                                </tr>
                                <tr>
                                    <td>Last Bonus</td>
                                    <td><span class="pr-3">:</span> 0.00</td>
                                </tr>
                                <tr>
                                    <td>Account Balance (USD)</td>
                                    <td><span class="pr-3">:</span> 0.00</td>
                                </tr>
                                </tbody>

                            </table>
                        </div><!-- /#profile -->

                        <!-- form change password -->
                        <div class="tab-pane" id="password">
                            <form method="POST" action="" role="form" id="frm-password">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group required has-feedback{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-current-password">Current password</label>
                                        <div class="w-100">
                                            <input type="password" class="form-control" id="input-current-password" name="current_password" placeholder="Current password" required>
                                            @if ($errors->has('current_password'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('current_password') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group required has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="input-password">New password</label>
                                        <div class="w-100">
                                            <input type="password" class="form-control" id="input-password" name="password" placeholder="New password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group required has-feedback">
                                        <label class="control-label" for="input-password-confirm">Confirm password</label>
                                        <div class="w-100">
                                            <input type="password" class="form-control" id="input-password-confirm" name="password_confirmation" placeholder="Confirm password" required>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="submit-save" class="btn btn-primary"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> Save</button>
                                    {{--<button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload_page()"><span class="fa fa-ban"></span> Close</button>--}}
                                </div>
                                {{--<input type="hidden" name="hidden_activeTab" id="hidden-activeTab" value="{{ !empty(session('activeTab')) ? session('activeTab') : '' }}">--}}
                            </form>
                        </div><!-- /#password -->

                        <!-- deposit -->
                        <div class="tab-pane" id="deposit">
                            <strong>BANK TRANSFER</strong>
                            <div class="card-deck mb-3">
                                <div class="card box-card bg-aba">
                                    <div class="img-hover-zoom customZoom d-block m-auto cursor-pointer" data-toggle="modal" data-target="#modal-deposit" onclick="modal_aba()">
                                        <img src="{{ asset('img/card/aba.png') }}" class="card-img-top" alt="ABA Bank">
                                    </div>
                                </div>
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto cursor-pointer" data-toggle="modal" data-target="#modal-deposit" onclick="modal_aceleda()">
                                        <img src="{{ asset('img/card/acleda.jpg') }}" class="card-img-top" alt="ACELEDA Bank">
                                    </div>
                                </div>
                            </div>
                            <strong>E-WALLETS TRANSFER</strong>
                            <div class="card-deck">
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto">
                                        <img src="{{ asset('img/card/pipay.jpg') }}" class="card-img-top" alt="Pi Pay">
                                    </div>
                                </div>
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto">
                                        <img src="{{ asset('img/card/wing.jpg') }}" class="card-img-top" alt="Wing">
                                    </div>
                                </div>
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto">
                                        <img src="{{ asset('img/card/true_money.jpg') }}" class="card-img-top" alt="True Money">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /#deposit -->

                        <!-- withdraw -->
                        <div class="tab-pane" id="withdraw">
                            <strong>BANK CARDS</strong>
                            <div class="card-deck">
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto">
                                        <img src="{{ asset('img/card/visa.jpg') }}" class="card-img-top" alt="Visa Card">
                                    </div>
                                </div>
                                <div class="card box-card">
                                    <div class="img-hover-zoom d-block m-auto">
                                        <img src="{{ asset('img/card/master.jpg') }}" class="card-img-top" alt="Master Card">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /#withdraw -->

                        <!-- statement -->
                        <div class="tab-pane" id="statement">
                            <div class="box-body">
                                <!-- Date and time range -->
                                <div class="form-group">
                                    <div class="input-group">
                                        <button type="button" class="btn btn-primary pull-right" id="daterange-btn">
                                            <i class="far fa-calendar-alt"></i>
                                            <span></span>
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.form group -->
                                <table class="table table-responsive">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Game</th>
                                        <th scope="col">Pool</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Number</th>
                                        <th scope="col">Bet Amount</th>
                                        <th scope="col">WinLoss</th>
                                        <th scope="col">Result</th>
                                        <th scope="col">GameID</th>
                                        <th scope="col">Odds</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="10" class="notFound">
                                            No record found!
                                            <hr>
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                         <th scope="row">1</th>
                                         <td>Mark</td>
                                         <td>Otto</td>
                                         <td>@mdo</td>
                                     </tr>--}}
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /#statement -->

                        <!-- bonus -->
                        <div class="tab-pane" id="bonus">
                            <strong>bonus</strong>
                        </div><!-- /#bonus -->

                    </div>
                    <!-- /tabs -->
                </div>
            </div>
        </div>

        <!-- Modal Deposit -->
        <div class="modal fade" id="modal-deposit" tabindex="-1" role="dialog" aria-labelledby="depositTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- date-range-picker -->
    <script type="text/javascript" src="{{ asset('components/moment/moment.min.js?v='.$version) }}"></script>
    <script type="text/javascript" src="{{ asset('components/bootstrap-daterangepicker/daterangepicker.js?v='.$version) }}"></script>
    <!-- bootstrap datepicker -->
    <script type="text/javascript" src="{{ asset('components/bootstrap-datepicker/bootstrap-datepicker.min.js?v='.$version) }}"></script>
    <!-- bootstrap time picker -->
    <script type="text/javascript" src="{{ asset('components/bootstrap-timepicker/bootstrap-timepicker.min.js?v='.$version) }}"></script>

    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            function onHashChange() {
                var hash = window.location.hash;

                if (hash) {
                    // using ES6 template string syntax
                    $(`[data-toggle="tab"][href="${hash}"]`).trigger('click');
                }
            }

            window.addEventListener('hashchange', onHashChange, false);
            onHashChange();
        });

    </script>
@endsection
