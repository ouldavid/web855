@extends('layouts.master')

@section('title', ucfirst($view_name))

@section('css')
    <!-- selectpicker css -->
    <link rel="stylesheet" href="{{ asset('components/bootstrap-select/bootstrap-select.min.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($view_name).'.css?v='.$version) }}">
@endsection

@section('content')
    <input type="hidden" name="hidden_id" data-detail-id="0" data-delete-id="0" data-row-id="0" value="0">
    <!-- Modal -->
    <div class="modal fade" id="messageModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="bgFull-gradient p-y-30">
        <div class="container">
            <div class="mb-3">
                <h1 class="text-center text-white">855 LOTTERY BETTING</h1>
                <small class="text-center text-white-50 d-block">check out our latest featured game! To meet today's challenges</small>
            </div>
            <div class="shadow mb-3" id="box-action">
                <div class="box-header row">
                    <div class="col-md-2 col-sm-3 col-6 mb-3">
                        <select class="selectpicker" data-width="fit">
                            <option data-content='<span class="flag-icon flag-icon-kh"></span> English'>Khmer</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-3 col-6 mb-3">
                        <select class="form-control" id="shift" name="shifts">
                            @if(isset($shifts))
                                @foreach($shifts as $shift)
                                    @if(time() > strtotime($setting->where('key','day_closing_time')->first()->value) and $shift->id == 2)
                                        <option selected="selected" value="{{$shift->id}}">{{$shift->shift}}</option>
                                    @else
                                        <option value="{{$shift->id}}">{{$shift->shift}}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-8 col-sm-6 col-12 mb-3">
                        <div id="divAccount" class="text-right">
                            <label>សម្យតុលទឹកប្រាក់ : </label>
                            <span>{{number_format($user->account->balance, 0, '.', ',')}}</span>
                        </div>
                    </div>
                </div>
                <div class="box-bg-gradient"></div>
                <div class="box-body row">
                    <div id="head-tabs">
                        <ul id="tabs" class="nav nav-tabs justify-content-center">
                            <li class="nav-item"><a href="" data-target="#digit" data-toggle="tab" class="nav-link text-uppercase font-weight-bold active">2D/3D/4D</a></li>
                            <li class="nav-item"><a href="" data-target="#animal" data-toggle="tab" class="nav-link text-uppercase font-weight-bold">ANIMAL</a></li>
                            <li class="nav-item"><a href="" data-target="#bigSmall" data-toggle="tab" class="nav-link text-uppercase font-weight-bold">BIG-SMALL</a></li>
                            <li class="nav-item"><a href="" data-target="#middleEdge" data-toggle="tab" class="nav-link text-uppercase font-weight-bold">MIDDLE/EDGE</a></li>
                        </ul>
                    </div>
                    <div id="tabsContent" class="tab-content">
                        <div id="digit" class="tab-pane fade active show mx-5">
                            <form>
                                <div class="form-group row mt-3">
                                    <div class="col-12">
                                        <div id="divPost">
                                            <ul>
                                                @foreach($posts as $post)
                                                    <li>
                                                        <div class="outer circle shapeborder">
                                                            <div class="inner circle shapeborder"><a href="#" rel="{{$post}}">{{$post}}</a></div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-3">
                                    <div class="col-4">
                                        <label for="bet-number" class="my-1">លេខចាក់</label>
                                        <input type="number" id="bet-number" class="form-control" pattern="^[0-9]" minlength="2" maxlength ="4"
                                               onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" required autofocus>
                                    </div>
                                    <div class="col-1 text-center">
                                        <br>
                                        <label id="bet-operator"></label>
                                    </div>
                                    <div class="col-3">
                                        <label for="bet-type" class="my-1">គុណ/អូស</label>
                                        <input type="number" id="bet-type" class="form-control" pattern="^[0-9]" maxlength ="4"
                                               onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" disabled>
                                    </div>
                                    <div class="col-4">
                                        <label for="bet-amount" class="my-1">ចំនួនទឹកប្រាក់</label>
                                        <input type="number" id="bet-amount" class="form-control" pattern="^[0-9]" min="100"
                                               onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <div id="keysControl">
                                            <ul class="d-flex justify-content-center">
                                                <li id="btn-new">
                                                    <div class="center-middle">
                                                        <label>វគ្គថ្មី</label>
                                                    </div>
                                                </li>
                                                <li id="btn-box" data-operator="box">
                                                    <div class="center-middle">
                                                        <label class="fas fa-times"></label>
                                                        <small>គុណ</small>
                                                    </div>
                                                </li>
                                                <li id="btn-roll-first" data-operator="roll-first">
                                                    <div class="center-middle">
                                                        <label class="fas fa-long-arrow-alt-up"></label>
                                                        <small>អូសក្បាល</small>
                                                    </div>
                                                </li>
                                                <li id="btn-roll-middle" data-operator="roll-middle">
                                                    <div class="center-middle">
                                                        <label class="fas fa-long-arrow-alt-left"></label>
                                                        <small>អុសកណ្តាល</small>
                                                    </div>
                                                </li>
                                                <li id="btn-roll-last" data-operator="roll-last">
                                                    <div class="center-middle">
                                                        <label class="fas fa-long-arrow-alt-down"></label>
                                                        <small>អូសកន្ទុយ</small>
                                                    </div>
                                                </li>
                                                <button id="btn-enter" class="bg-success text-white">
                                                    <div class="center-middle">
                                                        <label class="fas fa-level-down-alt"></label>
                                                        <small>បញ្ចូន</small>
                                                    </div>
                                                </button>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="animal" class="tab-pane fade">

                        </div>
                        <div id="bigSmall" class="tab-pane fade">

                        </div>
                        <div id="middleEdge" class="tab-pane fade">

                        </div>
                    </div>
                </div>
            </div>
            <div class="shadow" id="displayTable">
                <div class="mx-5">
                    {{--<h2 class="text-uppercase font-weight-bold text-center mb-3">DISPLAY</h2>--}}
                    <table id="bet-list" class="table">
                        <thead class="bg-gradient">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">ប៉ុស្ថិ៍</th>
                                <th scope="col">លេខចាក់/សត្វ</th>
                                <th scope="col">ចំនួន</th>
                                <th scope="col">ចំនួនសរុប</th>
                                <th scope="col">កែប្រែ</th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                    <hr>
                    <div id="display-footer">
                        <div id="total" class="row">
                            <div class="col-md-8 col-sm-8 col-6 text-right col-left">
                                <span>សរុប :</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-6 col-right">
                                <span class="color-red">0</span>
                            </div>
                        </div>
                        <div class="discount row">
                            <div class="col-md-8 col-sm-8 col-6 text-right col-left">
                                <span>កាត់ទឹក :</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-6 col-right">
                                <span>0</span>
                            </div>
                        </div>
                        <div id="grand-total" class="row">
                            <div class="col-md-8 col-sm-8 col-6 text-right col-left">
                                <span>សរុបទាំងអស់ :</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-6 col-right">
                                <span class="color-red bg-gray p-1">0</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-6 offset-md-8 offset-sm-8 offset-6 col-right mt-3">
                                <a href="#" id="btn-bet" class="btn btn-xs btn-default bg-primary text-white w-25" data-toggle="modal" data-target="#messageModal">ចាក់</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- selectpicker js -->
    <script type="text/javascript" src="{{ asset('components/bootstrap-select/bootstrap-select.min.js?v='.$version) }}"></script>
    {{--<!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {

            const message = {
                'required': 'Please input Data',
                'number': 'Please input only number',
                'length': 'Please input number more than this'
            };
            /** Post **/
            //select Post A as Default
            $('#divPost li a[rel=A]').parents('li').addClass('current');
            $('#divPost li a[rel=LOAB], #divPost li a[rel=LO4P]').parents('li').addClass('lo4p');
            //on Post selected
            $('#divPost li').click(function () {
                if($(this).hasClass('current')){
                    if($('#divPost li.current').length > 1) {
                        switch ($(this).find('a').attr('rel')) {
                            case 'LO4P':
                            case 'LOAB':
                            case '4P':
                                $('#divPost li').removeClass('current');
                                if($('#divPost li.current').length < 1){
                                    $('#divPost li a[rel=A]').parents('li').addClass('current');
                                }
                                break;
                            default:
                                $(this).removeClass('current');
                                $('#divPost li a[rel=4P]').parents('li.current').removeClass('current');
                                break;
                        }
                    }
                }
                else{
                    switch ($(this).find('a').attr('rel')) {
                        case 'LO4P':
                        case 'LOAB':
                            $('#divPost li.current').removeClass('current');
                            $(this).addClass('current');
                            break;
                        case '4P':
                            $('#divPost li').addClass('current');
                            $('#divPost li a[rel=LO4P]').parents('li.current').removeClass('current');
                            $('#divPost li a[rel=LOAB]').parents('li.current').removeClass('current');
                            break;
                        default:
                            $(this).addClass('current');
                            $('#divPost li a[rel=LO4P]').parents('li.current').removeClass('current');
                            $('#divPost li a[rel=LOAB]').parents('li.current').removeClass('current');
                            if($('#divPost li.current').length == 4){
                                $('#divPost li a[rel=4P]').parents('li').addClass('current');
                            }
                            break;
                    }
                }
            });

            /** Operator Button **/
            $("#keysControl li[data-operator]").click(function (e) {
                if($("#bet-number").val().length > 1){
                    $("#bet-operator").attr({'data-operator':$(this).attr('data-operator'), 'class':$(this).find('label').attr('class')});
                    let bet_type;
                    const bet_number = $('#bet-number').val();
                    switch ($(this).attr('data-operator')) {
                        case 'box':
                            bet_type = bet_number.length;
                            break;
                        case 'roll-first':
                            bet_type= bet_number.replace(bet_number.charAt(0), "9");
                            break;
                        case 'roll-middle':
                            bet_type= bet_number.replace(bet_number.charAt(1), "9");
                            break;
                        case 'roll-last':
                            bet_type= bet_number.substring(0, bet_number.length - 1) + "9";
                            break;
                    }
                    $('#bet-type').val(bet_type);
                }else{
                    $("#messageModal .modal-body strong").html(message.length);
                    $("#messageModal").modal('show');
                }
            });

            /** Ajax for Betting list **/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            /** Click Enter Button to add bet to show on table over **/
            $("#btn-enter").click(function(e){
                if($("#bet-number").val().length > 1 && $("#bet-amount").val().length >= 3 && $( "#divPost li" ).hasClass( "current" )){
                    let post = '';
                    let bet_type = '';
                    let id = $('#displayTable tbody tr').length;
                    const operator = $('#bet-operator').attr('data-operator');
                    const operator_class = $('#bet-operator').attr('class');
                    /*const bet_amount =
                        $("#bet-amount").val().charAt($("#bet-amount").val().length - 1) == '0' &&
                        $("#bet-amount").val().charAt($("#bet-amount").val().length - 2) == '0' ?
                            $("#bet-amount").val() : $("#bet-amount").val() + '00';*/

                    if (operator != '') {
                        bet_type = $("#bet-type").val();
                    }

                    $("#divPost li.current a").each(function () {
                        switch ($(this).attr('rel')) {
                            case 'LO4P':
                                post += 'LO,A,B,C,D,';
                                break;
                            case 'LOAB':
                                post += 'LO,A,B,';
                                break;
                            default:
                                if ($(this).attr('rel') != '4P') {
                                    post += $(this).attr('rel') + ',';
                                }
                                break;
                        }
                    });
                    const bet = {
                        shift: $('#shift').val(),
                        bet_detail_id: $("input[name=hidden_id]").data('detail-id') == 0 ? 0 : $("input[name=hidden_id]").data('detail-id'),
                        id: $("input[name=hidden_id]").attr('data-row-id') == 0 ? ++id : $("input[name=hidden_id]").attr('data-row-id'),
                        post: post.slice(0, -1),
                        bet_number: $("input[id=bet-number]").val(),
                        operator: operator,
                        operator_class: operator_class,
                        bet_type: bet_type,
                        bet_amount: $("#bet-amount").val()
                    };

                    $.ajax({
                        type: 'GET',
                        url: '{{ route('ajax-bet') }}',
                        data: bet,
                        success: function (data) {
                            if(data.errors) {
                                if(data.errors.bet_number || data.errors.bet_amount){
                                    $("#messageModal .modal-body strong").html(data.errors.bet_number ? data.errors.bet_number : data.errors.bet_amount);
                                    $("#messageModal").modal('show');
                                }
                            }
                            else {
                                /*if(data.closing_time){
                                    $("#homeModal").modal('show');
                                    if(data.closing_time== 1){
                                        $("#shift option").removeAttr('selected');
                                        $("#homeModal .modal-body").html('<p class="text-danger">ផុតកំណត់ម៉ោងចាក់ពេលថ្ងៃ!</p><p class="text-success">លោកអ្នកអាចចាក់បានតែពេលល្ងាចទេ!</p>');
                                        $("#shift option[value!="+data.closing_time+"]").attr('selected',true);
                                    }
                                    else{
                                        $("#homeModal .modal-body").html('<p class="text-danger">ផុតកំណត់ម៉ោងចាក់ពេលយប់!</p>');
                                    }
                                    clearAfterBet();
                                }
                                else{*/
                                const grand_total =
                                    parseInt($('#grand-total div:last-child span').text()) +
                                    (parseInt(data.sub_total) - (parseInt(data.sub_total) *
                                        (100 - parseInt(data.discount)) / 100)),

                                    balance = parseInt($("#divAccount span").text().replace(/[^0-9\.-]+/g, ""));
                                if (balance >= grand_total) {
                                    if ($("input[name=hidden_id]").attr('data-row-id') == 0) {
                                        $("#displayTable tbody").prepend(data.bet_list);
                                        setTotal(data.sub_total, data.discount, data.digit);
                                    } else {
                                        $('#displayTable tbody tr').each(function (index) {
                                            if ($(this).attr('data-row-id') == $("input[name=hidden_id]").attr('data-row-id')) {
                                                $(this).after(data.bet_list);
                                                deleteEntry($(this).children());
                                            }
                                        });
                                        $("input[name=hidden_id]").attr('data-row-id', '0');
                                        $("input[name=hidden_id]").attr('data-detail-id', '0');
                                        $("input[name=hidden_id]").attr('data-delete-id', '0');
                                    }

                                    clearText(['#bet-number','#bet-type','#bet-amount','#bet-operator']);
                                } else {
                                    /*$("#homeModal").modal('show');
                                    $("#homeModal .modal-body").html('<p class="text-danger">ទឹកប្រាក់របស់លោកអ្នកមិនគ្រប់គ្រាន់សម្រាប់ការចាក់នេះទេ!</p>'+
                                        '<p class="text-warning">ទឹកប្រាក់ដែលលោកអ្នកបានចាក់ <span class="text-danger">'+grand_total+'​៛</span>' +
                                        ' មានចំនួនច្រើនជាងសម្យតុលទឹកប្រាក់ឌែលនៅសល់ <span class="text-danger">'+balance+'​៛</sapn>!</p>');*/
                                }
                                //}
                            }
                        }
                    });
                    e.preventDefault();
                }
            });

            /** Bet Button **/
            $("#btn-bet").click(function(e){
                if($("#bet-list > tbody").children().length >= 1) {
                    const bet_data = [];
                    $('#bet-list tbody tr').each(function (index, tr) {
                        const bet_num = $(tr).find('td:eq(2)').text().split(' ');
                        bet_data.push({
                            bet_detail_id: $(tr).data("detail-id") !== undefined ? $(tr).data('detail-id') : 0,
                            post: $(tr).find('td:eq(1)').text(),
                            bet_number: bet_num[0],
                            operator: bet_num.length > 1 ? $(tr).find('td:eq(2)').attr('data-operator') : 'default',
                            bet_type: bet_num.length > 1 ? bet_num[2] : '',
                            bet_amount: parseInt($(tr).find('td:eq(3)').text().slice(0, -1).replace(/[^0-9\.-]+/g, "")),
                            sub_total: parseInt($(tr).find('td:eq(4)').text().slice(0, -1).replace(/[^0-9\.-]+/g, "")),
                            discount: $(tr).find("input:eq(1)").val()
                        });
                    });
                    const bet = {
                        bet_id: $("input[name=hidden_id]").val(),
                        delete_id: $("input[name=hidden_id]").attr('data-delete-id'),
                        balance: parseInt($("#divAccount p span").text()),
                        shift: $('select#shift').val(),
                        total: $("#total div:last-child span").text(),
                        grand_total: $("#grand-total div:last-child span").text(),
                        bet_data: JSON.stringify(bet_data)
                    };

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('store-bet') }}',
                        data: bet,
                        success: function (data) {
                            if (data.closing_time) {
                                $("#homeModal").modal('show');
                                if (data.closing_time == 1) {
                                    $("#messageModal .modal-body strong").html('ផុតកំណត់ម៉ោងចាក់ពេលថ្ងៃ!');
                                } else {
                                    $("#messageModal .modal-body strong").html('ផុតកំណត់ម៉ោងចាក់ពេលយប់!');
                                }
                                clearText(['#bet-list tbody', '#total div:last-child span', '.discount div span', '#grand-total div:last-child span']);
                            } else {
                                if (data.success) {
                                    $("#messageModal .modal-body").html('<p>' + data.msg + '</p>');
                                    $("#messageModal .modal-body").append('<span class="invisible">' + data.success + '</span>');
                                    $("#divAccount span").html((data.updated_balance).toLocaleString());
                                    clearText(['#bet-list tbody', '#total div:last-child span', '.discount div span', '#grand-total div:last-child span']);
                                } else {
                                    $("#messageModal .modal-body strong").html(data.msg);
                                    $("#messageModal").modal('show');
                                }
                            }
                        }
                    });
                    e.preventDefault();
                }
                else{
                    $("#messageModal .modal-body strong").html('You have not bet yet, so please enjoy our game!');
                    $("#messageModal").modal('show');
                }
            });

            function clearText(elements) {
                $.each(elements,function (key) {
                    if($(elements[key]).is("label")){
                        $(elements[key]).attr('class','');
                    }
                    else if($(elements[key]).is("input")){
                        $(elements[key]).val('');
                    }
                    else if($(elements[key]).is("span")){
                        $(elements[key]).text('0');
                    }
                    else{
                        $(elements[key]).text('');
                    }
                });
            }

            function setTotal(sub_total,discount_setting,digit){
                const discount_percentage = 100 - discount_setting;
                const discount = parseInt(sub_total) * discount_percentage / 100;
                const total = parseInt($('#total div:last-child span').text()) + parseInt(sub_total);
                const grand_total = parseInt($('#grand-total div:last-child span').text()) + (parseInt(sub_total) - discount);
                let new_discount = true;

                $("#total div:last-child span").html(total);
                $("#grand-total div:last-child span").html(grand_total);

                /** Display Discount **/
                if($(".discount div:last-child span").text() > 0){
                    $(".discount").each(function() {
                        if($(this).find("div:first-child span:last-child").text() == discount_percentage){
                            $(this).find("div:last-child span").html(parseInt($(this).find("div:last-child span").text()) + discount);
                            if($(this).find("div:first-child span:first-child").text() != digit)
                                $(this).find("div:first-child span:first-child").html("គ្រប់");
                            new_discount=false;
                        }
                    });
                }
                if(new_discount){
                    $(".discount").remove();
                    $("#total").after('' +
                        '<div class="discount row">' +
                            '<div class="col-md-8 col-sm-8 col-6 text-right col-left">' +
                                'កាត់ទឹក <span>'+digit+'</span>លេខ ' +
                                '<span>'+discount_percentage+'</span>% :' +
                            '</div>' +
                            '<div class="col-md-4 col-sm-4 col-6 col-right">' +
                                '<span>'+discount+'</span>' +
                            '</div>' +
                        '</div>');
                }
            }

            /** Delete row table bet **/
            function deleteEntry($this) {
                $($this).parents('tr').remove();
                $("#display-footer .row div:last-child span").text('0');
                $(".discount").remove();
                $("#displayTable tbody tr").each(function(index){
                    $(this).attr('data-delete-id',index+1);
                    setTotal(
                        $(this).find('td:eq(4)').text().slice(0, -1).replace(/[^0-9\.-]+/g, ""),
                        $(this).find("input:eq(1)").val(),
                        $(this).find("input:eq(2)").val()
                    );
                });

                // Get Id of delete rows
                const delete_id = $("input[name=hidden_id]").attr('data-delete-id') == 0 ? [] : [$("input[name=hidden_id]").attr('data-delete-id')];
                delete_id.push($($this).attr('data-detail-id'));
                $("input[name=hidden_id]").attr('data-delete-id',delete_id);

                //
                /*if($('#displayTable tbody tr').length < 1){
                    clearTable();
                }
                else{
                    $("#btn-bet").removeClass('isDisabled');
                }*/
            }
        });
    </script>
@endsection
