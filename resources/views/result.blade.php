@extends('layouts.master')

@section('title', ucfirst($view_name))

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($view_name).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container m-y-30">
        <div id="resultShow" class="mb-5">
            <h1>Latest Lottery Results</h1>
            <small>Check your result lotto online, find all the lotto winning numbers and see if you won the latest lotto jackpots!</small>

            <ul id="tabs" class="nav nav-tabs">
                <li class="nav-item"><a href="" data-target="#vietnam" data-toggle="tab" class="nav-link text-uppercase active">vietnam</a></li>
                <li class="nav-item"><a href="" data-target="#khmer" data-toggle="tab" class="nav-link text-uppercase">khmer</a></li>
                <li class="nav-item"><a href="" data-target="#thai" data-toggle="tab" class="nav-link text-uppercase">thai</a></li>
            </ul>
            <div id="tabsContent" class="tab-content">
                <div id="vietnam" class="tab-pane fade active show">
                    <table id="result-filter-table" class="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                            <th scope="col">ម៉ោង</th>
                            <th scope="col">ប្រភេទ</th>
                            <th scope="col">2D</th>
                            <th scope="col">3D</th>
                            <th scope="col">4D</th>
                            <th scope="col">សត្វ</th>
                            <th scope="col">ធំ/តូច</th>
                            <th scope="col">កណ្ដាល/ក្បាល កន្ទុយ</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div id="khmer" class="tab-pane fade">
                    <table class="table table-bordered table-dark">
                        <thead>
                            <tr>
                                <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                                <th scope="col">ម៉ោង</th>
                                <th scope="col">ប្រភេទ</th>
                                <th scope="col">A</th>
                                <th scope="col">B</th>
                                <th scope="col">C</th>
                                <th scope="col">D</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <!-- noon -->
                            <td rowspan="8" class="text-center">07.01.2020</td>
                            <td rowspan="4" class="text-center">4:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/monkey.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/horse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <!-- evening -->
                            <td rowspan="4" class="align-middle text-center">7:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/snake.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dog.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dragon.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/mouse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>

                        <!-- result yesterday -->

                        </tbody>
                    </table>
                </div>
                <div id="thai" class="tab-pane fade">
                    <table class="table table-bordered table-dark">
                        <thead>
                        <tr>
                            <th scope="col">ថ្ងៃចេញឆ្នោត</th>
                            <th scope="col">ម៉ោង</th>
                            <th scope="col">ប្រភេទ</th>
                            <th scope="col">A</th>
                            <th scope="col">B</th>
                            <th scope="col">C</th>
                            <th scope="col">D</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--<tr>
                            <!-- noon -->
                            <td rowspan="8" class="text-center">07.01.2020</td>
                            <td rowspan="4" class="text-center">4:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/monkey.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/chicken.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/horse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>--}}
                        {{--<tr>
                            <!-- evening -->
                            <td rowspan="4" class="align-middle text-center">7:30pm</td>
                            <td class="text-center">2D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4D</td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="text-right">
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_1.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_2.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_3.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                    <li><img class="img-fluid" src="{{ asset('img/results/number_4.png?v='.$version) }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">សត្វ</td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/snake.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dog.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/dragon.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="{{ asset('img/results/mouse.png') }}" alt="{{ $host_name }}" /></li>
                                </ul>
                            </td>
                        </tr>--}}

                        <!-- result yesterday -->

                        </tbody>
                    </table>
                </div>
            </div>
            <a href="#" class="moreResult">See all result</a>
        </div>

        <div id="reference">
            <h1 class="mb-3 text-left">Reference of Results</h1>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-12" id="noon">
                    <table id="day-result-table" class="table table-striped table-bordered">
                        {{--<thead class="bg-gradient">
                        <tr>
                            <th scope="col">Sunday<small class="text-white-50">(01-Jan-2020)</small></th>
                            <th scope="col">Tien Giang</th>
                            <th scope="col">Kien Giang</th>
                            <th scope="col">Da Lat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><p>Giải tám</p></td>
                            <td><p>12</p></td>
                            <td><p>12</p></td>
                            <td><p>12</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải bảy</p></td>
                            <td><p>123</p></td>
                            <td><p>123</p></td>
                            <td><p>123</p></td>
                        </tr>
                        <tr>
                            <td><span>Giải sáu</span></td>
                            <td>
                                <p>123</p>
                                <p>123</p>
                                <p>123</p>
                            </td>
                            <td>
                                <p>123</p>
                                <p>123</p>
                                <p>123</p>
                            </td>
                            <td>
                                <p>123</p>
                                <p>123</p>
                                <p>123</p>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải năm</p></td>
                            <td><p>1234</p></td>
                            <td><p>1234</p></td>
                            <td><p>1234</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải tư</p></td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải ba</p></td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                            <td>
                                <p>1234</p>
                                <p>1234</p>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải nhì</p></td>
                            <td><p>12345</p></td>
                            <td><p>12345</p></td>
                            <td><p>12345</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải nhất</p></td>
                            <td><p>12345</p></td>
                            <td><p>12345</p></td>
                            <td><p>12345</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải Đặc Biệt</p></td>
                            <td><p>123456</p></td>
                            <td><p>123456</p></td>
                            <td><p>123456</p></td>
                        </tr>
                        </tbody>--}}
                    </table>
                </div>
                <div class="col-md-4 col-sm-12 col-12" id="night">
                    <table id="night-result-table" class="table table-striped table-bordered">
                        {{--<thead class="bg-gradient">
                        <tr>
                            <th scope="col">Sunday<small class="text-white-50">(01-Jan-2020)</small></th>
                            <th scope="col">Hanoi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><p>Giải ĐB</p></td>
                            <td><p>12345</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải nhất</p></td>
                            <td><p>12345</p></td>
                        </tr>
                        <tr>
                            <td><p>Giải nhì</p></td>
                            <td>
                                <span>12345</span>
                                <span>12345</span>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải ba</p></td>
                            <td>
                                <div>
                                    <span>12345</span>
                                    <span>12345</span>
                                    <span>12345</span>
                                </div>
                                <div>
                                    <span>12345</span>
                                    <span>12345</span>
                                    <span>12345</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải năm</p></td>
                            <td>
                                <div>
                                    <span>1234</span>
                                    <span>1234</span>
                                    <span>1234</span>
                                </div>
                                <div>
                                    <span>1234</span>
                                    <span>1234</span>
                                    <span>1234</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải tư</p></td>
                            <td>
                                <div>
                                    <span>1234</span>
                                    <span>1234</span>
                                    <span>1234</span>
                                </div>
                                <div>
                                    <span>1234</span>
                                    <span>1234</span>
                                    <span>1234</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải sáu</p></td>
                            <td>
                                <span>123</span>
                                <span>123</span>
                                <span>123</span>
                            </td>
                        </tr>
                        <tr>
                            <td><p>Giải bảy</p></td>
                            <td>
                                <span>12</span>
                                <span>12</span>
                                <span>12</span>
                                <span>12</span>
                            </td>
                        </tr>
                        </tbody>--}}
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $.ajax({
                type: 'GET',
                url: '{{ route('result.show') }}',
                success: function (data) {
                    if(data.day_result != null) {
                        $('#day-result-table').html(data.day_result);
                        $('#night-result-table').html(data.night_result);
                        $('#result-filter-table tbody').html(data.result_data_filter);
                    }
                    else{
                        $('#divTab').html(data.msg);
                    }
                }
            });
        });
    </script>
@endsection
