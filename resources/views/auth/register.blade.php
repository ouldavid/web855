@extends('layouts.master')

@section('title', 'Register')

@section('css')
    <!-- intl tel input -->
    <link rel="stylesheet" href="{{ asset('plugins/intl-tel-input/css/intlTelInput.min.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/auth/register.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container m-y-30">
        <div class="row justify-content-center">
            <div class="col-md-5" id="content-register">
                <div class="card shadow">
                    <div class="card-header bg-gradient"><h3 class="text-center text-white text-uppercase">Register</h3></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <div class="position-relative">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full name" required autocomplete="name" autofocus>
                                    <i class="fas fa-user form-control-icon"></i>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            {{--<div class="form-group">
                                <div class="position-relative">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Username" required autocomplete="email">
                                    <i class="fas fa-at form-control-icon"></i>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>--}}

                            <div class="form-group">
                                <div class="position-relative">
                                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username">
                                    <i class="fas fa-at form-control-icon"></i>

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="position-relative">
                                    <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" maxlength="12" name="phone" value="{{ old('phone') }}" placeholder="Phone number" required>
                                    <i class="fas fa-phone form-control-icon"></i>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="position-relative">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                                    <i class="fas fa-lock form-control-icon"></i>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="position-relative">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required autocomplete="new-password">
                                    <i class="fas fa-lock form-control-icon"></i>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('plugins/intl-tel-input/js/intlTelInput.min.js?v='.$version) }}"></script>
    {{--<!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>--}}

@endsection
