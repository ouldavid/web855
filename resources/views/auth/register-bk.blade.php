@extends('layouts.master')

@section('title', 'Register')

@section('css')
    <!-- -->
    <link rel="stylesheet" href="{{ asset('plugins/intl-tel-input/css/intlTelInput.min.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/auth/register.css?v='.$version) }}">
@endsection

@section('content')

<div class="row justify-content-center m-y-30">
    <div class="col-md-5">
        <div class="card">
            <div class="card-header bg-gradient"><h3 class="text-center text-white text-uppercase">Register</h3></div>

            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <div class="position-relative">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Full name" required autocomplete="name" autofocus>
                            <i class="fas fa-user form-control-icon"></i>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    {{--<div class="form-group">
                        <div class="position-relative">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Username" required autocomplete="email">
                            <i class="fas fa-at form-control-icon"></i>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>--}}

                    <div class="form-group">
                        <div class="position-relative">
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username">
                            <i class="fas fa-at form-control-icon"></i>

                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="position-relative">
                            <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" maxlength="12" name="phone" value="{{ old('phone') }}" placeholder="Phone number" required>
                            <i class="fas fa-phone form-control-icon"></i>

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="position-relative">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                            <i class="fas fa-lock form-control-icon"></i>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="position-relative">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required autocomplete="new-password">
                            <i class="fas fa-lock form-control-icon"></i>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('plugins/intl-tel-input/js/intlTelInput.min.js?v='.$version) }}"></script>
    {{--<!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($view_name).'.js?v='.$version) }}"></script>--}}
    <script type="text/javascript">
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            // initialCountry: "auto",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            preferredCountries: ['kh'],
            separateDialCode: true,
            utilsScript: "{{ asset('plugins/intl-tel-input/js/utils.js') }}",
        });
        // validation format phone number
        /*$(function () {
            $('#phone').keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                $text = $(this);
                if (key !== 8 && key !== 9) {
                    if($text.val().length === 3) {
                        $text.val('('+$text.val()+') ');
                    }
                    if ($text.val().length === 9) {
                        $text.val($text.val() + '-');
                    }
                }
                return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
            })
        });*/

        /*function phone_formatting(ele,restore) {
            var new_number,
                selection_start = ele.selectionStart,
                selection_end = ele.selectionEnd,
                number = ele.value.replace(/\D/g,'');

            // automatically add dashes
            if (number.length > 2) {
                // matches: 123 || 123-4 || 123-45
                new_number = number.substring(0,3) + '-';
                if (number.length === 4 || number.length === 5) {
                    // matches: 123-4 || 123-45
                    new_number += number.substr(3);
                }
                else if (number.length > 5) {
                    // matches: 123-456 || 123-456-7 || 123-456-789
                    new_number += number.substring(3,6) + '-';
                }
                if (number.length > 6) {
                    // matches: 123-456-7 || 123-456-789 || 123-456-7890
                    new_number += number.substring(6);
                }
            }
            else {
                new_number = number;
            }

            // if value is heigher than 12, last number is dropped
            // if inserting a number before the last character, numbers
            // are shifted right, only 12 characters will show
            ele.value =  (new_number.length > 12) ? new_number.substring(0,12) : new_number;

            // restore cursor selection,
            // prevent it from going to the end
            // UNLESS
            // cursor was at the end AND a dash was added
            document.getElementById('msg').innerHTML='<p>Selection is: ' + selection_end + ' and length is: ' + new_number.length + '</p>';

            if (new_number.slice(-1) === '-' && restore === false
                && (new_number.length === 8 && selection_end === 7)
                || (new_number.length === 4 && selection_end === 3)) {
                selection_start = new_number.length;
                selection_end = new_number.length;
            }
            else if (restore === 'revert') {
                selection_start--;
                selection_end--;
            }
            ele.setSelectionRange(selection_start, selection_end);

        }

        function phone_number_check(field,e) {
            var key_code = e.keyCode,
                key_string = String.fromCharCode(key_code),
                press_delete = false,
                dash_key = 189,
                delete_key = [8,46],
                direction_key = [33,34,35,36,37,38,39,40],
                selection_end = field.selectionEnd;

            // delete key was pressed
            if (delete_key.indexOf(key_code) > -1) {
                press_delete = true;
            }

            // only force formatting is a number or delete key was pressed
            if (key_string.match(/^\d+$/) || press_delete) {
                phone_formatting(field,press_delete);
            }
            // do nothing for direction keys, keep their default actions
            else if(direction_key.indexOf(key_code) > -1) {
                // do nothing
            }
            else if(dash_key === key_code) {
                if (selection_end === field.value.length) {
                    field.value = field.value.slice(0,-1)
                }
                else {
                    field.value = field.value.substring(0,(selection_end - 1)) + field.value.substr(selection_end)
                    field.selectionEnd = selection_end - 1;
                }
            }
            // all other non numerical key presses, remove their value
            else {
                e.preventDefault();
//    field.value = field.value.replace(/[^0-9\-]/g,'')
                phone_formatting(field,'revert');
            }

        }

        document.getElementById('phone').onkeyup = function(e) {
            phone_number_check(this,e);
        }*/
    </script>
@endsection
