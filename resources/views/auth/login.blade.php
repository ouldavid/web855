@extends('layouts.master')

@section('title', 'Login')

@section('css')
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css?v='.$version) }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/auth/login.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container m-y-30">
        <!-- msg success -->
        @if(session()->has('flash_success'))
            <div id="msg-success" class="col-12 msg flash-message position-relative">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span class="glyphicon glyphicon-ok"></span> <strong>Congratulations your account has been created.</strong>
                    {{--<hr class="message-inner-separator">
                    <p>Your data was <strong class="text-success">Inserted</strong> successfully.</p>--}}
                </div>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-5" id="content-login">
                <div class="card shadow">
                    <div class="card-header bg-gradient"><h3 class="text-center text-white text-uppercase">{{ __('Login') }}</h3></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">
                                {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                                <div class="position-relative">
                                    <input id="login" type="text" class="form-control {{ $errors->has('username') || $errors->has('email') ? 'is-invalid' : '' }}" name="login" value="{{ old('username') }}" placeholder="Username" required autocomplete="username" autofocus>
                                    <i class="fas fa-user form-control-icon"></i>

                                    @if ($errors->has('username') || $errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                </span>
                                    @endif

                                    {{-- @error('username')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror--}}
                                </div>
                            </div>

                            <div class="form-group">
                                {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                                <div class="position-relative">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                                    <i class="fas fa-lock form-control-icon"></i>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 mb-3 text-xs-center">
                                    <div class="checkbox icheck">
                                        <label>
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                    {{--<div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>--}}
                                </div>
                                <div class="col-md-4 mb-3">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>

                            {{--<div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js?v='.$version) }}"></script>
    <script type="text/javascript">
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
@endsection
