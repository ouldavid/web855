<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreGameColumnToResultFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('result_filters', function (Blueprint $table) {
            $table->enum('animal', ['1'=>'mouse','2'=>'cow','3'=>'tiger','4'=>'rabbit','5'=>'dragon','6'=>'snake','7'=>'horse','8'=>'goat','9'=>'monkey','10'=>'chicken','11'=>'dog','12'=>'pig'])->after('4D')->nullable();
            $table->enum('big_small', ['1'=>'big','2'=>'small'])->after('animal')->nullable();
            $table->enum('middle_edge', ['1'=>'middle','2'=>'edge'])->after('big_small')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('result_filters', function (Blueprint $table) {
            //
        });
    }
}
