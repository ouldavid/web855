<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bet_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('bet_number');
			$table->integer('amount');
            $table->integer('bet_id');
			$table->string('type', 11)->default('default');
            $table->integer('discount');
            $table->integer('bet_multiply')->comment('Multiple of Post and count of Bet number');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bet_details');
	}

}
