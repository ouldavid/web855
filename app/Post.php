<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function post_settings()
    {
        return $this->hasMany(PostSetting::class);
    }

    public function bet_details()
    {
        return $this->belongsToMany(BetDetail::class);
    }

    public function result_filters()
    {
        return $this->hasMany(ResultFilter::class);
    }
}
