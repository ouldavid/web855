<?php

namespace App\Http\Controllers;

use App\Post;
use App\Result;
use App\ResultFilter;
use App\ResultProvince;
use App\ResultProvinceFilter;
use App\Setting;
use Carbon\Carbon;
use DOMDocument;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $this->getResult();
        return view('result');
    }

    function show(Request $request){
        $data=null; $msg=null; $data_result=null;
        $date=explode(' ',Result::max('created_at'));

        //Get Last 3 Results from DB
        $results = Result::whereDate('created_at', $date[0])->take(4)->get();
        //Get Result by Location type and if not yet release result will get the latest one
        $result_filters = ResultFilter::whereDate('created_at', $date[0])->get();
        if(count($result_filters) > 0) {
            $data = $this->getDataResultHTML($results, $result_filters);
        }
        else{
            $msg=
                '<div class="box">
                    <div class="box-body"><br/>
                        <code class="center-block"><h4>Have no Result in the Database yet, so please get the first Result to the system.</h4></code><br/>
                    </div>
                </div>';
            $data=['msg' => $msg];
        }
        return response()->json($data);
    }

    function getDataResultHTML($results,$result_filters){
        $day_province='';$night_province='';
        $day_result_data= array_fill(0, 18, null);; $night_result_data='';
        $prize= ['Giải ĐB','Giải nhất','Giải nhì','Giải ba','Giải tư','Giải năm','Giải sáu','Giải bảy','Giải tám'];
        foreach ($results as $key=>$result){
            $array_results = json_decode($result->result);
            if($result->shift_id == 1){
                $day_province .= '<th scope="col">' . $result->result_province->province . '</th>';
                foreach ($array_results as $index => $data) {
                    if ($index >= 2 and $index <= 4) {
                        $day_result_data [2] .= ($index == 2? '<td>':'').'<p>' . $data . '</p>'.($index == 4? '</td>':'');
                    } else if ($index >= 6 and $index <= 12) {
                        $day_result_data [6] .= ($index == 6? '<td>':'').'<p>' . $data . '</p>'.($index == 12? '</td>':'');
                    } else if ($index >= 13 and $index <= 14) {
                        $day_result_data [13] .= ($index == 13? '<td>':'').'<p>' . $data . '</p>'.($index == 14? '</td>':'');
                    } else {
                        $day_result_data [$index] .= '<td><p>' . $data . '</p></td>';
                    }
                }
            }else {
                $night_province .= '<th scope="col">' . $result->result_province->province . '</th>';
                foreach ($array_results as $index => $data) {
                    $night_result_data .= '
                        <tr>
                            <td><p>'.$prize[$index].'</p></td>
                            <td><p>'.$data.'</p></td>
                        </tr>';
                }
            }
        }
        $day_result_data = array_values(array_filter($day_result_data));
        $day_result =
            '<thead class="bg-gradient">
                <tr>
                    <th scope="col">'.date('l', strtotime($result->created_at)).'<small class="text-white-50">'.date('F j, Y', strtotime($result->created_at)).'</small></th>'.
                    $day_province
                .'</tr>
            </thead>
            <tbody>
                <tr>
                    <td><p>Giải tám</p></td>'.
                    $day_result_data[0]
                .'</tr>
                <tr>
                    <td><p>Giải bảy</p></td>'.
                    $day_result_data[1]
                .'</tr>
                <tr>
                    <td><span>Giải sáu</span></td>'.
                    $day_result_data[2]
                .'</tr>
                <tr>
                    <td><p>Giải năm</p></td>'.
                    $day_result_data[3]
                .'</tr>
                <tr>
                    <td><p>Giải tư</p></td>'.
                    $day_result_data[4]
                .'</tr>
                <tr>
                    <td><p>Giải ba</p></td>'.
                    $day_result_data[5]
                .'</tr>
                <tr>
                    <td><p>Giải nhì</p></td>'.
                    $day_result_data[6]
                .'</tr>
                <tr>
                    <td><p>Giải nhất</p></td>'.
                    $day_result_data[7]
                .'</tr>
                <tr>
                    <td><p>Giải Đặc Biệt</p></td>'.
                    $day_result_data[8]
                .'</tr>
            </tbody>';

        $night_result='
            <thead class="bg-gradient">
                <tr>
                    <th scope="col">'.date('l', strtotime($result->created_at)).'<small class="text-white-50">'.date('F j, Y', strtotime($result->created_at)).'</small></th>'.
                    $night_province
                .'</tr>
            </thead>
            <tbody>'.
                $night_result_data
            .'</tbody>';

        $result_data_filters=array();
        $day_first_data = true; $night_first_data = true;
        foreach ($result_filters as $key=>$result_filter){
            if($result_filter->post_id != 1) {
                $result_data_filters[$key] = '';
                if ($result_filter->result->shift_id == 1 )  {
                    if($day_first_data){
                        $day_first_data = false;
                        $result_data_filters[$key] .= '
                        <!-- noon -->
                        <td rowspan="11" class="text-center">' . date('F j, Y', strtotime($result_filter->created_at)) . '</td>
                        <td rowspan="4" class="text-center">4:30pm</td>';
                    }
                } else {
                    if($night_first_data){
                        $night_first_data = false;
                        $result_data_filters[$key] .= '
                        <!-- evening -->
                        <td rowspan="26" class="align-middle text-center">7:30pm</td>';
                    }
                }
                $result_data_filters[$key] .= '<td class="text-center">' . $result_filter->post->post . '</td>';
                for ($i = 0; $i < 6; $i++) {
                    if($i<3){
                        $digits[$i] = str_split($result_filter->{($i + 2) . "D"});
                        $result_data_filters[$key] .=
                            '<td>
                        <ul class="text-right">';
                        foreach ($digits[$i] as $digit) {
                            $result_data_filters[$key] .= '<li><img class="img-fluid" src="' . asset("img/results/number_" . $digit . ".png") . '" alt="' . $digit . '" /></li>';
                        }
                        $result_data_filters[$key] .= '</ul>
                    </td>';
                    }
                    else if($i==3){
                        $result_data_filters[$key] .=
                            '<td>
                                <ul class="animal text-center">
                                    <li><img class="img-fluid" src="' . asset("img/results/" . $result_filter->animal . ".png") . '" alt="' . $result_filter->animal . '" /></li>
                                </ul>
                            </td>';
                    }
                    else{
                        $result_data_filters[$key] .=
                            '<td>
                                <ul class="animal text-center">
                                    <li class="text-uppercase">'.$result_filter->{$i==4 ? 'big_small':'middle_edge'}.'</li>
                                </ul>
                            </td>';
                    }
                }
            }
        }
        $result_data_filter='';

        foreach ($result_data_filters as $key=> $result_data){
            $result_data_filter.= '<tr>'.$result_data.'</tr>';
        }

        $data = ['day_result' => $day_result, 'night_result' => $night_result, 'result_data_filter' => $result_data_filter];
        return $data;
    }

    /******* Get Result ******/
    function getResult(){
        date_default_timezone_set(env('APP_TIMEZONE'));
        $settings = Setting::all();
        $day_result_time = $settings->where('key',  'day_result_time')->first()->value;
        $day_result_source = $settings->where('key',  'day_result_source')->first()->value;
        $night_result_time = $settings->where('key',  'night_result_time')->first()->value;
        $night_result_source = $settings->where('key',  'night_result_source')->first()->value;

        //If Time is DAY_RESULT_TIME(16:45:00) < Current time
        $day = strtotime($day_result_time) < time() ? 0 : -1;

        //If Result Filter today not exist
        if(!(ResultFilter::whereDate('created_at', '=', Carbon::today()->addDay($day)->toDateString())->exists())) {
            $date = $this->getDayResult($day_result_source);
            $this->getResultFilter(1, date('N', strtotime($day.' days')),$date,$settings);

            /** Get Result Filters Function **/
            $msg = 'This is the updated Noon Result of Today';
        }
        else{
            $msg='This is the latest updated of today Noon Result';
        }

        $day = strtotime($night_result_time) < time() ? 0 : -1;

        if(!(ResultFilter::whereHas('result', function($item){
                $item->where('shift_id',  2);
            })
            ->whereDate('created_at', '=', Carbon::today()->addDay($day)
                ->toDateString())
            ->exists())) {

            $date = $this->getNightResults($night_result_source);
            $this->getResultFilter(2,date('N', strtotime($day.' days')), $date, $settings);

            $msg='This is the latest updated of all today Result';

        }

        return response()->json(array('msg'=> $msg), 200);
    }
    function getHtmlTableResult($url){
        $html = file_get_contents($url);

        /*** a new dom object ***/
        $dom = new domDocument;

        /*** load the html into the object ***/
        @$dom->loadHTML($html);

        /*** the table by its tag name ***/
        $tables = $dom->getElementsByTagName('tbody');

        return $tables;
    }
    function getDateResult($tables){
        /* Get date of result */
        $row = $tables->item(1)->getElementsByTagName('tr');
        $col = $row[0]->getElementsByTagName('td')->item(1)->nodeValue;
        $date = substr($col, -4) . '-' . substr($col, 3, 2) . '-' . substr($col, 0, 2) . ' ' . date("H:i:s");
        return $date;
    }
    function getDayResult($url){
        $tables = $this->getHtmlTableResult($url);
        $date = $this->getDateResult($tables);
        $results = array();
        for ($i = 0; $i < 3; $i++) {
            /*** loop over the table rows ***/
            foreach ($tables->item($i + 4)->getElementsByTagName('tr') as $key => $row) {

                /*** get each column by tag name ***/
                $cols = $row->getElementsByTagName('td');
                $str = trim($cols->item(0)->nodeValue);
                $n = strlen($str);
                if ($key == 0) $n = 1;

                switch ($n) {
                    case 10:
                    case 35:
                        $data = str_split($str, 5);
                        foreach ($data as $d) {
                            $results[$i][] = $d;
                        }
                        break;
                    case 12:
                        $data = str_split($str, 4);
                        foreach ($data as $d) {
                            $results[$i][] = $d;
                        }
                        break;
                    default:
                        $results[$i][] = $str;
                }
            }
        }

        //Insert Result to DB
        foreach ($results as $result) {
            $province = ResultProvince::where('province', $result[0])->first();

            //Remove Province and Province Code
            unset($result[0], $result[1]);
            //Reset Array Index
            $result = array_values($result);

            $data = Result::firstOrNew(['result' => json_encode($result)], ['result_province_id' => $province->id, 'shift_id'=> 1,'lottery_type_id' => 1]);
            $data->created_at = $date;
            $data->save();
        }

        return $date;
    }
    function getNightResults($url){
        $date_block = explode('<span class="txtngay">' , file_get_contents($url));
        $date_data= (explode('</span>',$date_block[1]));
        $date = date_format(date_create_from_format("j/m/Y",trim($date_data[0])),"Y-m-d").' '. date("H:i:s");

        $tables = $this->getHtmlTableResult($url);
        $string = trim(preg_replace("/[^A-Za-z0-9]/", ' ', $tables->item(2)->nodeValue));
        $results = explode(' ',preg_replace('/\s+/', ' ', $string));
        foreach ($results as $key=>$result){
            switch ($key){
                case 2:
                case 3:
                    $results[$key] = wordwrap($result , 5 , ' ' , true );
                    break;
                case 4:
                case 5:
                    $results[$key] = wordwrap($result , 4 , ' ' , true );
                    break;
                case 6:
                    $results[$key] = wordwrap($result , 3 , ' ' , true );
                    break;
                case 7:
                    $results[$key] = wordwrap($result , 2 , ' ' , true );
                    break;
            }
        }

        $data = Result::firstOrNew(['result' => json_encode($results)], ['result_province_id' => 22, 'shift_id'=> 2,'lottery_type_id' => 1]);
        $data->created_at = $date;
        $data->save();
        return $date;
    }
    function getResultFilter($shift_id, $day, $date, $settings){
        $province= ResultProvinceFilter::where([
            'shift_id'=> $shift_id,
            'weekly_id'=> $day
        ])->first();

        $result= Result::where('result_province_id','=',$province->result_province_id)->first();
        $results= json_decode($result->result);
        $post=5; /* 5 posts */

        $games= ['animal','big_small','middle_edge'];

        for($i=1;$i<=$post;$i++){
            if($i!=1){
                $data = new ResultFilter();
                $data->result_id= $result->id;
                $data->post_id = $i;
                $data->created_at= $date;
            }

            if($shift_id == 1){
                switch ($i){
                    case 1:
                        for($x=0;$x<count($results)-3;$x++){
                            $data = new ResultFilter();
                            $data->result_id= $result->id;
                            $data->post_id = $i;
                            $data->{'2D'} = substr($results[$x+2], -2);
                            $data->{'3D'} = substr($results[$x+2], -3);
                            foreach ($games as $game){
                                foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                    if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                        $data->$game = $setting->id;
                                    }
                                }
                            }
                            $data->created_at= $date;
                            $data->save();
                        }
                        break;
                    case 2:
                        $data->{'2D'} = $results[0];
                        $data->{'3D'} = $results[1];
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;
                    case 3:
                        $data->{'2D'} = substr(end($results), 1, -3);
                        $data->{'3D'} = substr(end($results), -3);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;
                    case 4:
                        $data->{'2D'} = substr(end($results), -2);
                        $data->{'3D'} = substr(end($results), 1, -2);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;
                    case 5:
                        $data->{'2D'} = substr(end($results), 1, -4).substr(end($results), -1);
                        $data->{'3D'} = substr(end($results), 2, -1);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;
                }
            }else{
                switch ($i){
                    case 1:
                        for($x=1;$x<count($results)-2;$x++){
                            $result_data=explode(' ',$results[$x]);
                            foreach ($result_data as $item){
                                $data = new ResultFilter();
                                $data->result_id= $result->id;
                                $data->post_id = $i;
                                $data->{'2D'} = substr($item, -2);
                                $data->{'3D'} = substr($item, -3);

                                foreach ($games as $game){
                                    foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                        if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                            $data->$game = $setting->id;
                                        }
                                    }
                                }
                                $data->created_at= $date;
                                $data->save();
                            }
                        }
                        break;

                    case 2:
                        $result_data= array();
                        for($x=6;$x<count($results);$x++) {
                            $result_data[$x-6] = explode(' ', $results[$x]);
                        }
                        for($y=0;$y< count($result_data[1]); $y++){
                            $data = new ResultFilter();
                            $data->result_id= $result->id;
                            $data->post_id = $i;
                            $data->{'2D'} = $result_data[1][$y];
                            $data->{'3D'} = array_key_last($result_data[1]) != $y ? $result_data[0][$y]:null;
                            foreach ($games as $game){
                                foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                    if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                        $data->$game = $setting->id;
                                    }
                                }
                            }
                            $data->created_at= $date;
                            $data->save();
                        }
                        break;

                    case 3:
                        $data->{'2D'} = substr($results[0], 0,2);
                        $data->{'3D'} = substr($results[0], -3);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;

                    case 4:
                        $data->{'2D'} = substr($results[0], -2);
                        $data->{'3D'} = substr($results[0], 0, -2);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;

                    case 5:
                        $data->{'2D'} = substr($results[0], 0, -4).substr($results[0], -1);
                        $data->{'3D'} = substr($results[0], 1, -1);
                        foreach ($games as $game){
                            foreach (json_decode($settings->where('key',$game)->first()->value) as $setting){
                                if(in_array(($game == 'big_small' ? $this->digSum($data->{'2D'}): $data->{'2D'}),$setting->value)){
                                    $data->$game = $setting->id;
                                }
                            }
                        }
                        $data->save();
                        break;
                }
            }
        }
    }

    function digSum( $n)
    {
        $sum = 0;

        // Loop to do sum while
        // sum is not less than
        // or equal to 9
        while($n > 0 || $sum > 9)
        {
            if($n == 0)
            {
                $n = $sum;
                $sum = 0;
            }
            $sum += $n % 10;
            $n = (int)$n / 10;
        }
        return $sum;
    }
}
