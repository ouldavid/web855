<?php

namespace App\Http\Controllers;

use App\Bet;
use App\BetDetail;
use App\Post;
use App\PostSetting;
use App\Shift;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use function foo\func;

class BettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        // user access
        if( !isset($user )) {
            return redirect()->route('login');
        }

        $shifts= Shift::all();
        $setting = Setting::all();

        $posts= $this->getPostCollection();

        return view('betting',compact('user','shifts','posts','setting'));
    }

    function getPostCollection(){
        $all_posts=Post::all();
        $posts=new Collection();
        foreach($all_posts as $post){
            if($post->id==1){
                switch ($all_posts->count()){
                    case 3:
                        $posts->push($all_posts[0]->post.$all_posts[1]->post.$all_posts[2]->post);
                        break;
                    case 5:
                        $posts->push($all_posts[0]->post.'4P');
                        $posts->push($all_posts[0]->post.$all_posts[1]->post.$all_posts[2]->post);
                        $posts->push('4P');
                }
            }
            else
                $posts->push($post->post);
        }

        return $posts;
    }

    /** Return Bet data to show on home page *
     * @param Request $request
     * @return JsonResponse
     */
    function ajaxBet(Request $request){
        $validator = Validator::make($request->all(), [
            'bet_number' => ['required','numeric','digits_between:2,4' ],
            'bet_amount' => ['required','numeric','digits_between:1,7'],
        ]);

        if($validator->passes()) {
            $data = ['closing_time' => $request->shift];
            $settings = Setting::all();
            $posts = Post::all();
            $closing_time = $settings->where('key', ($request->shift == 1 ? 'day' : 'night') . '_closing_time')->first()->value;
            if (time() < strtotime($closing_time)) {
                $user = auth()->user();
                $bet = $user->bets
                    ->where('shift_id', $request->shift)
                    ->whereBetween('created_at', [$request->date . ' 00:00:00', $request->date . ' 23:59:59'])->first();

                $digit = $this->getDigit($request);
                $post_amount = $this->getPostAmount($posts, $request, $digit);
                $bet_num = $this->getBetNumber($request, $digit);
                $sub_total = intval($request->bet_amount) * intval($post_amount) * count($bet_num);
                //dd($sub_total);
                $setting = json_decode($settings
                    ->where('key', 'bet_discount')->first()->value);

                $bet_list = '
            <tr data-detail-id="' . $request->bet_detail_id . '" data-row-id="' . $request->id . '">
                <td data-column="No">1</td>
                <td data-column="ប៉ុស្ថិ៍">' . $request->post . '</td>
                <td data-column="លេខចាក់/សត្វ" data-operator="' . $request->operator . '">' . $request->bet_number . ($request->operator_class != "" ? ' <i class="' . $request->operator_class . '"></i> ' . $request->bet_type : "") . '</td>
                <td data-column="ចំនួន">' . number_format($request->bet_amount, 0, '.', ',') . '៛</td>
                <td data-column="ចំនួនសរុប">' . number_format($sub_total, 0, '.', ',') . '៛</td>
                <td data-column="កែប្រែ">
                    <a href="javascript:void(0)" onclick="editEntry(this,' . $request->id . ')" class="btn btn-xs btn-default bg-primary text-white"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="javascript:void(0)" onclick="deleteEntry(this)" class="btn btn-xs btn-default bg-danger text-white" data-button-type="delete"><i class="glyphicon glyphicon-remove"></i></a>
                    <input type="hidden" value="' . $setting->{$digit . 'd'} . '">
                    <input type="hidden" value="' . $digit . '">
                </td>
            </tr>';

                $data = [
                    'bet_id' => (isset($bet)) ? $bet->id : 0,
                    'bet_list' => $bet_list,
                    'sub_total' => $sub_total,
                    'discount' => $setting->{$digit . 'd'},
                    'digit' => $digit
                ];
            }
            return response()->json($data);
        }else{
            return response()->json(['errors' => $validator->errors()]);
        }
    }

    /** Insert all Bet data to Bet, Bet Detail and Bet Detail Post table. *
     * @param Request $request
     * @return JsonResponse
     */
    function store(Request $request){
        $data=['closing_time'=>$request->shift];
        $posts = Post::all();
        $settings= Setting::all();
        $closing_time= $settings->where('key',($request->shift == 1 ? 'day':'night').'_closing_time')->first()->value;
        if(time() < strtotime($closing_time)){
            $user = auth()->user();
            $last_data = $user->bets->where('id', $request->bet_id)->first();
            $updated_balance = 0;
            $updated = false;
            if (isset($last_data)) {
                $updated_balance = intval($user->account->balance) + intval($last_data->grand_total) - intval($request->grand_total);
                /* Message when updated Success */
                $msg = "ការកែប្រែលើសន្លឹកឆ្នោតរបស់លោកអ្នកទទួលបានជោគជ័យ! សូមរក្សាកន្ទុយសំបុត្រដើម្បីផ្ទៀងផ្ទាត់លេខឆ្នោត។ សូមអរគុណ!";
                $updated = true;
            } else {
                $updated_balance = intval($user->account->balance) - intval($request->grand_total);
                /* Message when add new Success */
                $msg = "ការចាក់ឆ្នោតរបស់លោកអ្នកទទួលបានជោគជ័យ! សូមរក្សាកន្ទុយសំបុត្រដើម្បីផ្ទៀងផ្ទាត់លេខឆ្នោត។ សូមអរគុណ!";
            }

            if ($updated_balance > 0) {
                /* Update Account Balance */
                //$updated_balance = intval($user->account->balance) + $last_grand_total - intval($request->grand_total);
                $user->account->update(['balance' => $updated_balance]);

                /* Insert Data to Bet table */
                $bet = Bet::updateOrCreate(
                    ['id' => $request->bet_id, 'user_id' => $user->id, 'lottery_type_id' => 1],
                    ['shift_id' => $request->shift, 'total' => $request->total, 'grand_total' => $request->grand_total]
                );
                /* Insert data to Bet Detail table */
                foreach (json_decode($request->bet_data) as $bet_data) {
                    $bet_num = $this->getBetNumber($bet_data, $this->getDigit($bet_data));
                    $post_id = $this->getPostID($posts, $bet_data->post);
                    $bet_multiply = intval($this->getPostAmount($posts, $bet_data, $this->getDigit($bet_data), $request->shift)) * count($bet_num);
                    $bet_detail = BetDetail::updateOrCreate(
                        ['id' => $bet_data->bet_detail_id, 'bet_id' => $bet->id],
                        ['bet_number' => json_encode($bet_num), 'amount' => $bet_data->bet_amount, 'type' => $bet_data->operator, 'discount' => $bet_data->discount, 'bet_multiply' => $bet_multiply
                        ]);
                    $bet_detail->posts()->sync($post_id);
                }
                $delete_id = explode(',', $request->delete_id);
                if ($delete_id[0] != 0) {
                    BetDetail::destroy($delete_id);
                }
                $data = [
                    'ticket' => $bet->ticket,
                    'updated_balance' => $updated_balance,
                    'success' => true,
                    'updated' => $updated,
                    'msg' => $msg
                ];
            } else {
                $data = ['msg' => 'ទឹកប្រាក់របស់លោកអ្នកមិនគ្រប់គ្រាន់សម្រាប់ការចាក់នេះទេ!'];
            }
        }
        return response()->json($data);
    }

    function ajaxOnChange(Request $request){
        $user = auth()->user();
        $bet = $user->bets
            ->where('shift_id',$request->shift)
            ->where('ticket',$request->ticket)
            ->whereBetween('created_at', [$request->date.' 00:00:00', $request->date.' 23:59:59'])->first();
        $bet_list= array();
        $sub_total = array();
        $setting = array();
        $bet_id=0;
        $time='';
        if(isset($bet)){
            $bet_id= $bet->id;
            $time= $bet->created_at->diffInMinutes(Carbon::now());
            foreach ($bet->bet_details as $id => $bet_data) {
                $bet_numbers = json_decode($bet_data->bet_number);
                $sub_total[] = intval($bet_data->amount) * intval($bet_data->bet_multiply);
                $setting[] = ['digit'=> strlen($bet_numbers[0]), 'discount' => $bet_data->discount];
                $bet_number= implode(array_unique(str_split(implode($bet_numbers))));
                $posts='';
                switch ($bet_data->type){
                    case 'box':
                        $bet_number .= ' x '.strlen($bet_numbers[0]);
                        break;
                    case 'default':
                        $bet_number= $bet_numbers[0];
                        break;
                    default:
                        if($bet_data->type == 'roll-first') $operator = '↑';
                        else if($bet_data->type == 'roll-middle') $operator = '←';
                        else $operator = '↓';

                        $bet_number = $bet_numbers[0].' '.$operator.' '.$bet_numbers[count($bet_numbers)-1];
                        break;
                }
                foreach ($bet_data->posts as $index => $post){
                    $posts .= $post->post.''.($index < count($bet_data->posts)-1 ? ',': '');
                }

                $bet_list[] =
                    '<tr data-detail-id="'.$bet_data->id.'" data-row-id="'.($id+1).'">
                        <td data-column="No">1</td>
                        <td data-column="ប៉ុស្ថិ៍">' . $posts . '</td>
                        <td data-column="លេខចាក់/សត្វ" data-operator="'.$bet_data->type.'">' . $bet_number . '</td>
                        <td data-column="ចំនួន">' . number_format($bet_data->amount, 0, '.', ',') . '៛</td>
                        <td data-column="ចំនួនសរុប">' . number_format($sub_total[$id], 0, '.', ',') . '៛</td>
                        <td data-column="កែប្រែ">
                            <a href="javascript:void(0)" onclick="editEntry(this,'.$id.')" class="btn btn-xs btn-default bg-primary text-white"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="javascript:void(0)" onclick="deleteEntry(this)" class="btn btn-xs btn-default bg-danger text-white" data-button-type="delete"><i class="glyphicon glyphicon-remove"></i></a>
                            <input type="hidden" value="'.$setting[$id]["discount"].'">
                            <input type="hidden" value="'.$setting[$id]["digit"].'">
                        </td>
                    </tr>';
            }
        }

        $data=[
            'bet_id' => $bet_id,
            'bet_list'=>$bet_list,
            'sub_total'=> $sub_total,
            'setting' => $setting,
            'over_time' => $time > 10 ? true:false,
            'success' => count($bet_list) > 0 ? true: false
        ];

        return response()->json($data);
    }

    function ajaxTicket(Request $request){
        $user = auth()->user();
        $bets=
            $user->bets
                ->whereBetween('created_at', [$request->date.' 00:00:00', $request->date.' 23:59:59'])
                ->where('shift_id',$request->shift)
                ->filter(function($bets) use ($request){
                    if(!empty($request->ticket))
                        return $bets->ticket == $request->ticket;
                    else
                        return '*';
                });

        return response()->json($bets);
    }

    /** Return Digit of Bet number*
     * @param $request
     * @return int
     */
    function getDigit($request){
        if($request->operator == 'box')
            return $request->bet_type;
        else
            return strlen($request->bet_number);

    }
    /** Return Bet Number *
     * @param Request $request
     * @param $digit
     * @return array
     */
    function getBetNumber($request,$digit){
        switch ($request->operator){
            case 'roll-first':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type,str_pad(1, $digit, 0)));
                break;
            case 'roll-middle':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type,str_pad(1, $digit-1, 0)));
                break;
            case 'roll-last':
                $bet_num = array_map('strval',range($request->bet_number, $request->bet_type));
                break;
            case 'box':
                $bet_num = $this->permute($request->bet_number,(strlen($request->bet_number)-$digit)+1);
                break;
            default:
                $bet_num=[$request->bet_number];
                break;
        }
        foreach ($bet_num as $key=>$bet){
            if(strlen($request->bet_type) > 1 and strlen($bet) < strlen($request->bet_number)){
                $bet_num[$key] = str_pad($bet, strlen($request->bet_number), '0', STR_PAD_LEFT);
            }
        }
        return $bet_num;
    }

    /** Return collection Post ID by collection Post *
     * @param $posts
     * @param $post_name
     * @return Collection
     */
    function getPostID($posts, $post_name){
        /** Collect Post from Post Table by selected post on bet form **/
        $posts= $posts->whereIn('post', explode(",", $post_name));

        /** Collect Post ID for each Betting **/
        $post_id=new Collection();
        foreach ($posts as $post)
            $post_id->push($post->id);
        return $post_id;
    }

    /** Return Post Amount *
     * @param $posts
     * @param $request
     * @param $digit
     * @param string $shift
     * @return int
     */
    function getPostAmount($posts, $request, $digit, $shift = '' ){
        $post_amount=0;
        $post_lo=false;

        $post_id= $this->getPostID($posts,$request->post);
        /** Get post setting by shift and post **/
        $settings = PostSetting::where('shift_id', $shift =='' ? $request->shift : $shift)
            ->whereIn('post_id', $post_id)->get();

        /** Get number of Post Amount **/
        foreach ($settings as $setting){
            if ($setting->post_id==1) {
                $post_amount += $setting->{$digit . 'D'};
                $post_lo = true;
            }
            else {
                $post_amount += $setting->{$digit.'D'};

                /* if Post LO will plus 3D */
                if($post_lo==true and $digit == 2)
                    $post_amount += $setting->{'3D'};
            }
        }
        return $post_amount;
    }

    /** Permutation number by n digit *
     * @param $arg
     * @param $n
     * @return array
     */
    function permute($arg,$n) {
        $array = is_string($arg) ? str_split($arg) : $arg;
        if(count($array) === $n)
            return $array;
        $result = array();
        foreach($array as $key => $item)
            foreach($this->permute(array_diff_key($array, array($key => $item)),$n) as $p)
                $result[] = $item . $p;
        return array_values(array_unique($result));
    }

}
