<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostSetting extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function lottery_type()
    {
        return $this->belongsTo(LotteryType::class);
    }
}
