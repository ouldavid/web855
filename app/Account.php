<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'balance'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public  function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
