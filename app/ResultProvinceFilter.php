<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultProvinceFilter extends Model
{
    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function weekly()
    {
        return $this->belongsTo(Weekly::class);
    }

    public function result_province()
    {
        return $this->belongsTo(ResultProvince::class);
    }
}
