<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultFilter extends Model
{
    public function result(){
        return $this->belongsTo(Result::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
