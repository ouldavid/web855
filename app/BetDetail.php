<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetDetail extends Model
{
    protected $fillable = [
        'bet_number','amount','bet_id','type','discount','bet_multiply'
    ];

    public function bet()
    {
        return $this->belongsTo(Bet::class);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
